<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Home Personas</title>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}/assets/css/custom.css" />">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.all.min.js"></script>
    </head>
    <body>
        <h1>Tabla listado personas</h1><span><a href="<c:out value="${pageContext.request.contextPath}/personas/formulario.htm"/>">Ingresar Persona</a></span>
        <div class="clearfix"></div>
        <hr>
        
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Rut</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Direccion</th>
                    <th>Telefono</th>
                    <th>Email</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${personas}" var="persona">
                    <tr>
                        <td><c:out value="${persona.idPersona}"/></td>
                        <td><c:out value="${persona.rut}"/></td>
                        <td><c:out value="${persona.nombre}"/></td>
                        <td><c:out value="${persona.apellido}"/></td>
                        <td><c:out value="${persona.direccion}"/></td>
                        <td><c:out value="${persona.telefono}"/></td>
                        <td><c:out value="${persona.email}"/></td>
                        <td><a href="<c:out value="${pageContext.request.contextPath}/personas/formulario.htm?id=${persona.idPersona}"/>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                        <td><a href="<c:out value="${pageContext.request.contextPath}/personas/delete.htm?id=${persona.idPersona}"/>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        
    </body>
</html>
