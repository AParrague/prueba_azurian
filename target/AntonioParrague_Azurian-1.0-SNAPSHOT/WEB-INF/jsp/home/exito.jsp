<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Bienvenido </title>
    </head>
    <body>
        <h1>Exito</h1>
        <h2><c:out value="Bienvenido ${nombre} ${apellido}"/></h2>
        <p><c:out value="Id: ${id}"/></p>
        <p>Los siguentes usuarios tambien han ingresado a esta pagina</p>
        <table>
            <thead>
                <tr>
                    <th>Rut</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Direccion</th>
                    <th>Telefono</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    
                </tr>
            </tbody>
        </table>
    </body>
</html>
