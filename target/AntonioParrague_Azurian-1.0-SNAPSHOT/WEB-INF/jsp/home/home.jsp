<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Bienvenido</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    </head>
    <body>
        <h1>Bienvenido</h1>
        <form:form commandName="persona" method="post">
            <form:errors path="*" element="div" cssClass="alert alert-danger"  />
            
            <form:label path="rut" >Rut</form:label>
            <form:input path="rut" />
            <form:errors path="rut"/>
            <br/>
            <form:label path="nombre">Nombre</form:label>
            <form:input path="nombre" />
            <form:errors path="nombre" />
            <br/>
            <form:label path="apellido">Apellido</form:label>
            <form:input path="apellido" />
            <form:errors path="apellido" />
            <br/>
            <form:label path="direccion">Direccion</form:label>
            <form:input path="direccion" />
            <form:errors path="direccion" />
            <br/>
            <form:label path="telefono">Telefono</form:label>
            <form:input path="telefono" />
            <form:errors path="telefono" />
            <br/>
            <form:label path="email">Email</form:label>
            <form:input path="email" />
            <form:errors path="email" />
            <br/>
            <input type="submit" value="Enviar"/>
        </form:form>
    </body>
</html>
