package com.aparrague.antonioparrague_azurian.DAO;

import java.util.List;
import com.aparrague.antonioparrague_azurian.model.Persona;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class DAO_Persona {
    private JdbcTemplate jdbcTemplate;
    
    public DAO_Persona() {
        Conexion con = new Conexion();
        this.jdbcTemplate = new JdbcTemplate(con.conectar());
    }
    
    public List getAll(){
        String query = "Select * From Persona";
        return this.jdbcTemplate.queryForList(query);
    }
    public Persona getById(String id){
        String query = "Select * From Persona Where idPersona = ?";
        return (Persona) this.jdbcTemplate.queryForObject(
                query
                , new Object[] { id }
                , new BeanPropertyRowMapper(Persona.class));
    }
    public int upsert(Persona p){
        return this.jdbcTemplate.update("REPLACE INTO persona(idPersona, rut, nombre, apellido, direccion, telefono, email)"
                + "VALUES(?,?,?,?,?,?,?)",
                p.getId(), p.getRut(), p.getNombre(), p.getApellido(), p.getDireccion(), p.getTelefono(), p.getEmail());
        
    }
    
    public int delete(String id){
        return this.jdbcTemplate.update(
                "DELETE FROM persona WHERE idPersona = ? ",
                id);
    }
}
