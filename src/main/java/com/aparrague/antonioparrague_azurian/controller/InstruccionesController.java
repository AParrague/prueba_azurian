package com.aparrague.antonioparrague_azurian.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("instrucciones.htm")
public class InstruccionesController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView instrucciones(){
        return new ModelAndView("instrucciones");
    }
}
