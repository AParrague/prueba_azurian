package com.aparrague.antonioparrague_azurian.controller;

import com.aparrague.antonioparrague_azurian.DAO.DAO_Persona;
import com.aparrague.antonioparrague_azurian.model.Persona;
import com.aparrague.antonioparrague_azurian.model.ValidarPersona;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("personas.htm")
public class PersonasController {
    private ValidarPersona validarPersona;
    public PersonasController() {
        this.validarPersona = new ValidarPersona();
    }
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView home(){
        Persona persona = new Persona();
        ModelAndView mav = new ModelAndView("personas/home");
        mav.addObject("personas", persona.getAll());
        return mav;
    }
    @RequestMapping(value = "/formulario", method = RequestMethod.GET)
    public ModelAndView formulario(HttpServletRequest request){
        Persona persona = new Persona();
        String id = request.getParameter("id");
        if(id != null){
            persona = persona.getById(id);
        }
        return new ModelAndView("personas/formulario", "persona", persona);
    }
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute Persona p, BindingResult result, SessionStatus status){
        validarPersona.validate(p, result);
        ModelAndView mav;
        if(result.hasErrors()){
            mav = new ModelAndView();
            mav.setViewName("personas/formulario");
            mav.addObject("persona", new Persona());
            return mav;
        } else {
            DAO_Persona persona = new DAO_Persona();
            int asd = persona.upsert(p);
            mav = new ModelAndView("redirect:/personas.htm");
            return mav;
        }
    }
    @RequestMapping(value="/delete.htm", method = RequestMethod.GET)
    public ModelAndView delete(HttpServletRequest request){
        String id = request.getParameter("id");
        Persona persona = new Persona();
        persona.delete(id);
        ModelAndView mav = new ModelAndView("redirect:/personas.htm");
        return mav;
    }
    
    @ModelAttribute("paisesLista")
    public Map<String, String> listadoPaises(){
        Map<String, String> pais = new LinkedHashMap<>();
        pais.put("1", "Chile");
        pais.put("2", "Brasil");
        pais.put("3", "Argentina");
        pais.put("4", "España");
        return pais;
    }
}
