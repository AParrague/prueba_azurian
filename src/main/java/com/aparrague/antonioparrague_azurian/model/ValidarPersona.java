package com.aparrague.antonioparrague_azurian.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class ValidarPersona implements Validator{
    private static final String CARACTERES_ESPECIALES = "[^a-zA-Z0-9]";
    private static final String RUT_PATTERN = "^0*(\\d{1,3}(\\.?\\d{3})*)\\-?([\\dkK])$";
    private static final String EMAIL_PATTERN = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$";
    private static final String TELEFONO_PATTERN = "^(\\+?56)?(\\s?)(0?9)(\\s?)[98765]\\d{7}$";
    
    private Pattern pattern;
    private Matcher matcher;
    
    @Override
    public boolean supports(Class<?> type) {
        return Persona.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Persona persona = (Persona) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rut", "required.rut", "El campo Rut es Obligatorio");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre", "required.nombre", "El campo Nombre es Obligatorio");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "apellido", "required.apellido", "El campo Apellido es Obligatorio");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "direccion", "required.direccion", "El campo Direccion es Obligatorio");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefono", "required.telefono", "El campo Telefono es Obligatorio");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required.email", "El campo Email es Obligatorio");
        
        this.pattern = Pattern.compile(RUT_PATTERN);
        this.matcher = this.pattern.matcher(persona.getRut());
        String rut = persona.getRut().replaceAll(CARACTERES_ESPECIALES, "");
        if(!matcher.matches()){
            errors.rejectValue("rut", "rut.incorrect", "El rut " + persona.getRut() + " ingresado no es valido");
        } else if(!validarRut(rut)){
            errors.rejectValue("rut", "rut.incorrect", "El rut " + persona.getRut() + " ingresado no es valido");
        } else {
            persona.setId(Integer.parseInt(rut.substring(0, rut.length() - 1)));
        }
        this.pattern = Pattern.compile(TELEFONO_PATTERN);
        this.matcher = this.pattern.matcher(persona.getTelefono());
        if(!matcher.matches()){
            errors.rejectValue("telefono", "telefono.incorrect", "El telefono " + persona.getTelefono() + " ingresado no es valido");
        }
        this.pattern = Pattern.compile(EMAIL_PATTERN);
        this.matcher = this.pattern.matcher(persona.getEmail());
        if(!matcher.matches()){
            errors.rejectValue("email", "email.incorrect", "El email " + persona.getEmail() + " ingresado no es valido");
        }
    }
    public boolean validarRut(String rut) {
        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }
}
