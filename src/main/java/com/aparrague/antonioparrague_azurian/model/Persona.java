package com.aparrague.antonioparrague_azurian.model;

import com.aparrague.antonioparrague_azurian.DAO.DAO_Persona;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;

public class Persona {
    private int id;
    private String rut;
    private String nombre;
    private String apellido;
    private String direccion;
    private String email;
    private String telefono;
    
    public Persona() {
    }

    public Persona(String rut, String nombre, String apellido, String direccion, String email, String telefono) {
        this.rut = rut;
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.email = email;
        this.telefono = telefono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    public List<Persona> getAll(){
        DAO_Persona persona = new DAO_Persona();
        System.out.println(persona.getAll());
        return persona.getAll();
    }
    
    public Persona getById(String id){
        DAO_Persona persona = new DAO_Persona();
        return persona.getById(id);
    }
    
    public int delete(String id){
        DAO_Persona persona = new DAO_Persona();
        return persona.delete(id);
    }
}
