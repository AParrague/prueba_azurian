<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Bienvenido</title>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}/assets/css/custom.css" />">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.all.min.js"></script>
    </head>
    <body>
        <h1>Bienvenido</h1>
        <form:form commandName="persona" method="post">
            <form:errors path="*" element="div" cssClass="alert alert-danger" action="save.htm"/>
            
            <form:label path="rut" >Rut</form:label>
            <form:input path="rut" />
            <br/>
            <form:label path="nombre">Nombre</form:label>
            <form:input path="nombre" />
            <br/>
            <form:label path="apellido">Apellido</form:label>
            <form:input path="apellido" />
            <br/>
            <form:label path="direccion">Direccion</form:label>
            <form:input path="direccion" />
            <br/>
            <form:label path="telefono">Telefono</form:label>
            <form:input path="telefono" />
            <br/>
            <form:label path="email">Email</form:label>
            <form:input path="email" />
            <br/>
            <input type="submit" value="Enviar"/>
        </form:form>
    </body>
</html>
